#ifndef PSPSYSCON_H_
#define PSPSYSCON_H_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void sceSysconGetBaryonVersion(unsigned int *baryon);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // PSPSYSCON_H_
