/*
    procompat.prx : a compatibility layer between infinity and procfw
    Copyright (C) 2015 David "Davee" Morgan

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version. 

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <pspsdk.h>
#include <pspinit.h>

#include <string.h>

// procfw
#include <rebootex_conf.h>

#define DEVKIT_VER  (0x06060110)

typedef int (* INSERT_BTCNF)(char *new_mod, char *before_mod, BtcnfHeader *header, int *size, u16 flags);
typedef int (* REPLACE_BTCNF)(char *new_mod, char *replace_mod, BtcnfHeader *header, int *size);

void *memset(void *b, int c, size_t len)
{
	void *pRet = b;
	unsigned char *ub = (unsigned char *) b;

	while(len > 0)
	{
		*ub++ = (unsigned char) c;
		len--;
	}

	return pRet;
}

int entry(BtcnfHeader *btcnf, int btcnf_size, INSERT_BTCNF insert_btcnf, REPLACE_BTCNF replace_btcnf, int model, int is_recovery)
{
    // clear location for configuration and set default akin to CIPL
    memset((void *)REBOOTEX_CONFIG, 0, 0x200);
    
    rebootex_config *conf = (rebootex_config *)(REBOOTEX_CONFIG);
    conf->magic = REBOOTEX_CONFIG_MAGIC;
    conf->psp_model = model;
    conf->rebootex_size = 0;
    conf->psp_fw_version = DEVKIT_VER;
    
    if (is_recovery)
    {
        replace_btcnf("/vsh/module/_recovery.prx", "/vsh/module/vshmain.prx", btcnf, &btcnf_size);
    }
    
    insert_btcnf("/kd/_systemctrl.prx", "/kd/init.prx", btcnf, &btcnf_size, (BOOTLOAD_VSH | BOOTLOAD_GAME | BOOTLOAD_POPS | BOOTLOAD_UPDATER | BOOTLOAD_UMDEMU | BOOTLOAD_APP | BOOTLOAD_MLNAPP));
    insert_btcnf("/kd/_vshctrl.prx", "/kd/vshbridge.prx", btcnf, &btcnf_size, (BOOTLOAD_VSH));
    
    // return the new btcnf size
    return btcnf_size;
}
